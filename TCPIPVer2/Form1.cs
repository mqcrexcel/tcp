﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace TCPIPVer2
{
    public partial class Form1 : Form
    {
        TcpClient clientSocket = new TcpClient();
        NetworkStream severStream = default(NetworkStream);
        string readdata = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clientSocket.Connect(textBox1.Text, Int32.Parse(textBox2.Text));

            Thread ctTheard = new Thread(getMessage);
            ctTheard.Start();
        }

        private void getMessage()
        {
            string returndata;
            while (true)
            {
                severStream = clientSocket.GetStream();
                var buffsize = clientSocket.ReceiveBufferSize;
                byte[] instream = new byte[buffsize];

                severStream.Read(instream, 0, buffsize);

                returndata = System.Text.Encoding.ASCII.GetString(instream);

                readdata = returndata;
                msg();

            }

        }

        private void msg()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(msg));
            }
            else
            {
                textBox4.Text = readdata;

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            byte[] outsteam = Encoding.ASCII.GetBytes(textBox3.Text);
            severStream.Flush();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
